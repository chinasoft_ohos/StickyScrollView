# StickyScrollView

## 项目介绍
项目名称：StickyScrollView

所属系列：openharmony的第三方组件适配移植

功能：粘连头部,悬停底部的ScrollView

项目移植状态：主功能完成

调用差异：无

开发版本：sdk6，DevEco Studio2.2 Beta1

基线版本：Release 1.0.2

## 效果演示

![输入图片说明](https://images.gitee.com/uploads/images/2021/0525/171841_5a987d56_2291343.gif "SVID_20210525_171502_1[1].gif")

## 安装教程
1.在项目根目录下的build.gradle文件中，

 ```

allprojects {

    repositories {

        maven {

            url 'https://s01.oss.sonatype.org/content/repositories/releases/'

        }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```

 dependencies {

    implementation('com.gitee.chinasoft_ohos:StickyScrollView:1.0.3')

    ......  

 }

 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明

1.将仓库导入到本地仓库中

2.在布局文件中加入StickyScrollView控件,代码实例如下:
```
   <com.frank.library.StickyScrollView
            ohos:id="$+id:scrollView"
            ohos:height="match_parent"
            ohos:width="match_parent">

```
3.在Ability的onStart方法中设置头部尾部控件和状态监听：
```   
        //设置头部控件     
        scrollView.setHeadView(headView);

        //设置尾部控件
        scrollView.setFootView(footView);

        //设置scrollview滚动监听
        scrollView.setStickChangeLisener(new StickChangeLisener() {
            @Override
            public void stcikHead() {
                 //头部悬停状态
            }

            @Override
            public void hideHead() {
                 //头部隐藏状态
            }

            @Override
            public void stickFoot() {
                 //尾部悬停状态
            }

            @Override
            public void hideFoot() {
                 //尾部隐藏状态
            }
        });

```

## 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


## 版本迭代
- 1.0.3
- 0.0.1-SNAPSHOT

## 版权和许可信息
```

    MIT License

    Copyright (c) 2017 Amar Jain

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
```