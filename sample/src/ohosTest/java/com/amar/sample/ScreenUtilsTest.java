package com.amar.sample;

import com.amar.library.ScreenUtils;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import ohos.app.Context;

import org.junit.Test;

/**
 * ScreenUtils单元测试
 *
 * @author zf
 * @since 2021-04-23
 */
public class ScreenUtilsTest {
    private Context context;

    /**
     * 获取上下文
     */
    public void initClass() {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().getContext();
    }

    /**
     * 获取屏幕实际高度
     */
    @Test
    public void getScreenHeight() {
        initClass();
        int screenHeight = ScreenUtils.getScreenHeight(context);
    }

    /**
     * 获取屏幕高度（不含导航栏）
     */
    @Test
    public void getAttributesHeight() {
        initClass();
        int attributesHeight = ScreenUtils.getAttributesHeight(context);
    }
}